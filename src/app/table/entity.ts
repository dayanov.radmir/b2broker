import { ChildDTO, Color, EntityDTO } from '@b2b/server'
import { Type } from 'class-transformer'


export class Child implements ChildDTO {
  id!: string
  color!: Color
}

export class Entity implements EntityDTO {
  @Type(() => Child)
    child!: Child

  id!: string
  int!: number
  float!: number
  color!: Color
}
