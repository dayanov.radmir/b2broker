import { effect, Injectable, signal } from '@angular/core'
import { plainToClass } from 'class-transformer'

import { ApiService } from '../api.service'
import { ControlsService } from '../controls/controls.service'
import { Entity } from './entity'


@Injectable({
  providedIn: 'root',
})
export class TableService {
  entities = signal<Entity[]>([])
  constructor(
    private controlsService: ControlsService,
    private apiService: ApiService,
  ) {
    effect(() => {
      const entitiesDTO = this.apiService.entitiesDTO()
      const entitiesToDisplay: string[] = this.controlsService.arrayIds()
        .filter((id) => +id <= entitiesDTO.length)
        .reduce(
          (acc: string[], id) => {
            if (!acc.includes(id)) {
              acc.push(id)
            }
            return acc
          },
          [],
        )
      for (let i=entitiesDTO.length; i > 0; i--) {
        if (entitiesToDisplay.length >= 10) {
          break
        }

        const id = `${i}`
        if (!entitiesToDisplay.includes(id)) {
          entitiesToDisplay.push(id)
        }
      }
      this._mapEntities(entitiesToDisplay)
    }, { allowSignalWrites: true })
  }

  private _mapEntities(ids: string[]): void {
    const entitiesDTO = this.apiService.entitiesDTO()
    this.entities.set(
      ids.sort((a, b) => +a - +b)
        .map((id) => {
          const itemIndex = +id - 1
          const entity = entitiesDTO[itemIndex]
          return plainToClass(Entity, entity)
        }),
    )
  }
}
