import { Injectable, signal } from '@angular/core'
import { EntityDTO } from '@b2b/server'

import { ClientSocket } from './mediator/client-socket'


@Injectable({
  providedIn: 'root',
})
export class ApiService {
  entitiesDTO = signal<EntityDTO[]>([])

  constructor(private socket: ClientSocket) {
    this.socket.addEventListener('message', ({ data }) => {
      if (data.type === 'Entities') {
        this.entitiesDTO.set(data.data)
      }
    })
  }

  updateArraySize(size: number): void {
    this.socket.send({
      type: 'Entities Amount',
      amount: size,
    })
  }

  updateInterval(timeout: number): void {
    this.socket.send({
      type: 'Interval Timeout',
      timeout,
    })
  }
}
