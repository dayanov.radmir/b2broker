import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'

import { AppComponent } from './app.component'
import { ControlsModule } from './controls/controls.module'
import { TableComponent } from './table/table.component'


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    ControlsModule,
    TableComponent,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {
}
