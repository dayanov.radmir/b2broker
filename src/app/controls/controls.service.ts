import { effect, Injectable, signal } from '@angular/core'

import { ApiService } from '../api.service'


@Injectable({
  providedIn: 'root',
})
export class ControlsService {
  arrayIds = signal<string[]>([])
  timeoutInterval = signal<number>(300)
  arraySize = signal<number>(1_000)

  constructor(private apiService: ApiService) {
    effect(() => {
      this.apiService.updateArraySize(this.arraySize())
    })

    effect(() => {
      this.apiService.updateInterval(this.timeoutInterval())
    })
  }
}
