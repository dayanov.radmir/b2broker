import { ComponentFixture, TestBed } from '@angular/core/testing'

import { AdditionalIdsComponent } from './additional-ids.component'


describe('AdditionalIdsComponent', () => {
  let component: AdditionalIdsComponent
  let fixture: ComponentFixture<AdditionalIdsComponent>

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdditionalIdsComponent],
    })
    fixture = TestBed.createComponent(AdditionalIdsComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
