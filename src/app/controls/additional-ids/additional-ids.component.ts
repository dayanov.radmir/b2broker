import { ChangeDetectionStrategy, Component, forwardRef, OnDestroy, OnInit } from '@angular/core'
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR } from '@angular/forms'
import { map, noop, Subscription } from 'rxjs'


type IDs = string[]

@Component({
  selector: 'app-additional-ids',
  templateUrl: './additional-ids.component.html',
  styleUrls: ['./additional-ids.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{ provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => AdditionalIdsComponent), multi: true }],
})
export class AdditionalIdsComponent implements ControlValueAccessor, OnInit, OnDestroy {
  control: FormControl<string | null> = new FormControl('')

  private _subscriptions: Subscription[] = []

  private _onChange: (ids: IDs) => void = noop
  private _onTouched: () => void = noop

  ngOnInit(): void {
    this._subscriptions.push(
      this.control.valueChanges
        .pipe(map((ids) => ids?.match(/\d+/g) || []))
        .subscribe((ids) => this._onChange(ids)),
    )
  }

  ngOnDestroy(): void {
    this._subscriptions.forEach((s) => s.unsubscribe())
  }

  onInput(event: KeyboardEvent): void {
    switch (event.key) {
      case 'Backspace':
      case 'Delete':
      case 'ArrowLeft':
      case 'ArrowRight':
      case 'ArrowUp':
      case 'ArrowDown':
        return
      case ',':
        (this.control.value?.endsWith(',') || this.control.value?.endsWith(' ')) && event.preventDefault()
        break
      case ' ':
        this.control.value?.endsWith(' ') && event.preventDefault()
        break
      default:
        !event.key.match(/[\d\s,]/) && event.preventDefault()
    }
  }

  registerOnChange(fn: (ids: IDs) => void): void {
    this._onChange = fn
  }

  registerOnTouched(fn: () => void): void {
    this._onTouched = fn
  }

  setDisabledState(isDisabled: boolean): void {
    this.control[isDisabled ? 'disable' : 'enable']()
  }

  writeValue(ids: IDs | null): void {
    this.control.setValue(ids?.join(', ') || '')
  }
}
