import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { AdditionalIdsComponent } from './additional-ids/additional-ids.component'
import { ControlsComponent } from './controls.component'


@NgModule({
  declarations: [AdditionalIdsComponent, ControlsComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [ControlsComponent],
})
export class ControlsModule { }
