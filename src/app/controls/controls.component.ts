import { ChangeDetectionStrategy, Component } from '@angular/core'

import { ControlsService } from './controls.service'


@Component({
  selector: 'app-controls',
  templateUrl: './controls.component.html',
  styleUrls: ['./controls.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ControlsComponent {
  constructor(public controlsService: ControlsService) {
  }
}
