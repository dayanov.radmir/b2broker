/// <reference lib="webworker" />

import { MockServer } from '@b2b/server'

import { ServerSocket } from './server-socket'


const serverSocket = new ServerSocket()
const server = new MockServer(serverSocket)
