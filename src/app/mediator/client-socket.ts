import { Injectable } from '@angular/core'
import { Messages, Socket } from '@b2b/server'


@Injectable({ providedIn: 'root' })
export class ClientSocket implements Socket {
  private _server: Worker = new Worker(new URL('./server.worker', import.meta.url))
  private _listeners: Array<(event: { data: Messages }) => void> = []

  constructor() {
    this._server.addEventListener('message', (event: MessageEvent) => {
      this._listeners.forEach((cb) => cb(event))
    })
    this.send({ type: 'Start Timer' })
  }

  send(msg: Messages): void {
    this._server.postMessage(msg)
  }

  addEventListener(_event: 'message', cb: (event: { data: Messages }) => void): void {
    this._listeners.push(cb)
  }

  close(): void {
    this._listeners.length = 0
    this._server.terminate()
  }
}
