import { Messages, Socket } from '@b2b/server'


export class ServerSocket implements Socket {
  private _listeners: Array<(event: { data: Messages }) => void> = []

  constructor() {
    addEventListener('message', (event: MessageEvent) => {
      this._listeners.forEach((cb) => cb(event))
    })
  }

  addEventListener(_event: 'message', cb: (event: { data: Messages }) => void): void {
    this._listeners.push(cb)
  }

  close(): void {
    this._listeners.length = 0
  }

  send(msg: Messages): void {
    postMessage(msg)
  }
}
