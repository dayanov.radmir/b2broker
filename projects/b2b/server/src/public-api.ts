export { Messages } from './lib/messages'
export { MockServer } from './lib/mock-server'
export { Socket } from './lib/socket'
export { EntityDTO, ChildDTO, Color } from './lib/entity.interface'
