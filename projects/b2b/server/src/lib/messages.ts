import { EntityDTO } from './entity.interface'


export interface Message<T extends string = string> {
  type: T;
}

export interface UpdateEntitiesAmount extends Message {
  type: 'Entities Amount';
  amount: number;
}

export interface UpdateTimeout extends Message {
  type: 'Interval Timeout';
  timeout: number;
}

export interface StartTimer extends Message {
  type: 'Start Timer';
}

export interface StopTimer extends Message {
  type: 'Stop Timer';
}

export interface UpdateEntities extends Message {
  type: 'Entities';
  data: EntityDTO[];
}

export type Messages = UpdateEntitiesAmount | UpdateTimeout | StartTimer | StopTimer | UpdateEntities
