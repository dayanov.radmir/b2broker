import { fakeAsync, tick } from '@angular/core/testing'
import { noop } from 'rxjs'

import { Messages } from './messages'
import { MockServer } from './mock-server'
import { Socket } from './socket'
import objectContaining = jasmine.objectContaining
import Spy = jasmine.Spy


class MockSocket implements Socket {
  private _subscribers: Array<(event: { data: Messages }) => void> = []
  addEventListener(_: 'message', cb: (event: { data: Messages }) => void): void {
    this._subscribers.push(cb)
  }
  send(msg: Messages): void {
    this._subscribers.forEach((cb) => cb({ data: msg }))
  }
  mockSend(msg: Messages): void {
    this._subscribers.forEach((cb) => cb({ data: msg }))
  }
  close = noop
}

describe('MockServer', () => {
  let socket: MockSocket
  let addListenerSpy: Spy
  let sendSpy: Spy

  beforeEach(() => {
    socket = new MockSocket()
    sendSpy = spyOn(socket, 'send').and.callThrough()
    addListenerSpy = spyOn(socket, 'addEventListener').and.callThrough()
  })

  it('should subscribe to socket', () => {
    new MockServer(socket)
    expect(addListenerSpy).toHaveBeenCalledOnceWith('message', jasmine.anything())
  })

  describe('UpdateEntitiesAmount', () => {
    let entities: Array<unknown> = []

    beforeEach(() => {
      socket.addEventListener('message', ({ data }) => {
        if (data.type === 'Entities') {
          entities = data.data
        }
      })
      new MockServer(socket)
      socket.mockSend({ type: 'Entities Amount', amount: 200 })
    })

    it('should send generated entities', () => {
      expect(sendSpy).toHaveBeenCalledOnceWith(objectContaining({ type: 'Entities' }))
    })

    it('should send 200 entities', () => {
      expect(entities.length).toEqual(200)
    })
  })

  it('should start timer', fakeAsync(() => {
    new MockServer(socket)
    const msg = { type: 'Start Timer' } as const
    socket.mockSend(msg)
    tick(300)
    expect(sendSpy).toHaveBeenCalledWith(objectContaining({ type: 'Entities' }))
    expect(sendSpy).toHaveBeenCalledTimes(2)
    socket.mockSend({ type: 'Stop Timer' })
  }))

  describe('UpdateTimeout', () => {
    it('should emit 2 events after interval updated to 500ms', fakeAsync(() => {
      new MockServer(socket)

      socket.mockSend({ type: 'Interval Timeout', timeout: 500 })
      socket.mockSend({ type: 'Start Timer' })

      tick(500)

      expect(sendSpy).toHaveBeenCalledWith(objectContaining({ type: 'Entities' }))
      expect(sendSpy).toHaveBeenCalledTimes(2)

      socket.mockSend({ type: 'Stop Timer' })
    }))
  })
})
