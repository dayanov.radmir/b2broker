import { Messages } from './messages'


export interface Socket {
  close(): void;
  send(msg: Messages): void;
  addEventListener(event: 'message', cb: (event: {data: Messages}) => void): void;
}
