import { delay, EMPTY, merge, Observable, of, switchMap, take, tap } from 'rxjs'
import { TestScheduler } from 'rxjs/testing'

import { Timer } from './timer'
import clock = jasmine.clock


describe('Timer', () => {
  let timer: Timer
  let testScheduler: TestScheduler

  beforeEach(() => {
    timer = new Timer()
    testScheduler = new TestScheduler((actual, expected) => {
      expect(actual).toEqual(expected)
    })
    clock().install()
  })

  afterEach(() => clock().uninstall())

  it('should emit first event right after the start', () => {
    testScheduler.run(({ expectObservable }) => {
      expectObservable(timer.start().pipe(take(2))).toBe('a 299ms (b|)', { a: 0, b: 1 })
    })
  })

  describe('Interval change', () => {
    const changeInterval$ = (delayMs: number, intervalMs: number, tick: number): Observable<never> => of(1).pipe(
      delay(delayMs),
      tap(() => {
        clock().tick(tick)
        timer.updateInterval(intervalMs)
      }),
      switchMap(() => EMPTY),
    )

    beforeEach(() => clock().mockDate(new Date()))

    it('should increase interval to 500ms', () => {
      testScheduler.run(({ expectObservable }) => {
        const source$ = timer.start().pipe(take(4))
        const update$ = changeInterval$(400, 500, 100)
        expectObservable(merge(source$, update$)).toBe('a 299ms b 499ms a 499ms (b|)', { a: 0, b: 1 })
      })
    })

    describe('Interval reduced', () => {
      it('should emit event immediately after the interval change', () => {
        testScheduler.run(({ expectObservable }) => {
          const source$ = timer.start().pipe(take(4))
          const update$ = changeInterval$(200, 100, 200)
          expectObservable(merge(source$, update$)).toBe('a 199ms a 99ms b 99ms (c|)', { a: 0, b: 1, c: 2 })
        })
      })

      it('should emit event with delay after the change', () => {
        testScheduler.run(({ expectObservable }) => {
          const source$ = timer.start().pipe(take(4))
          const update$ = changeInterval$(100, 200, 100)
          expectObservable(merge(source$, update$)).toBe('a 199ms a 199ms b 199ms (c|)', { a: 0, b: 1, c: 2 })
        })
      })
    })
  })
})
