export type Color = string

export interface Child {
  color: Color;
}

export interface ChildDTO extends Child {
  id: string;
}

export interface Entity {
  int: number;
  float: number;
  color: Color;
}

export interface EntityDTO extends Entity {
  id: string;
  child: ChildDTO;
}
