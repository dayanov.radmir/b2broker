import { Subscription } from 'rxjs'

import { EntityDTO } from './entity.interface'
import { EntityGenerator } from './generator'
import { Messages } from './messages'
import { Socket } from './socket'
import { Timer } from './timer'


export class MockServer {
  private readonly _timer = new Timer()
  private readonly _generator = new EntityGenerator()

  private _timerSubscription: Subscription | undefined

  constructor(private readonly channel: Socket) {
    this.channel.addEventListener('message', ({ data: msg }) => this._handleMessage(msg))
  }

  private _handleMessage(cmd: Messages): void {
    switch (cmd.type) {
      case 'Start Timer':
        this._startTimer()
        break

      case 'Stop Timer':
        this._timerSubscription && this._timerSubscription.unsubscribe()
        break

      case 'Interval Timeout':
        this._timer.updateInterval(cmd.timeout)
        break

      case 'Entities Amount': {
        const entities = this._generator.updateEntitiesAmount(cmd.amount)
        this._sendUpdate(entities)
        break
      }
    }
  }

  private _startTimer(): void {
    this._timerSubscription = this._timer.start().subscribe(() => {
      this._sendUpdate(this._generator.updateEntities())
    })
  }

  private _sendUpdate(entities: EntityDTO[]): void {
    this.channel.send({
      type: 'Entities',
      data: entities,
    })
  }
}
