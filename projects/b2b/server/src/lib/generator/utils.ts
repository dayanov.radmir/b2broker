import { faker } from '@faker-js/faker'

import { Child, Entity } from '../entity.interface'


export function generateEntity(): Omit<Entity, 'child'> {
  const color = generateColor()
  const int = faker.number.int()
  const float = faker.number.float({ precision: 0.000_000_000_000_000_001 })
  return {
    int,
    float,
    color,
  }
}

export function generateChild(): Child {
  return {
    color: generateColor(),
  }
}

const colorFn: Array<() => string> = [
  (): string => faker.color.human().replace(/\s/g, ''),
  faker.color.rgb.bind(faker, { format: 'hex' }) as () => string,
  faker.color.rgb.bind(faker, { format: 'css' }) as () => string,
]

export function generateColor(): string {
  return faker.helpers.arrayElement(colorFn)()
}
