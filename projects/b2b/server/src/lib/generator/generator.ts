import { faker } from '@faker-js/faker'

import { EntityDTO } from '../entity.interface'
import { generateChild,generateEntity } from './utils'


export class EntityGenerator {
  private _requestedEntitiesAmount = 0
  private readonly _entities: EntityDTO[] = []

  updateEntitiesAmount(amount: number): EntityDTO[] {
    this._requestedEntitiesAmount = amount
    if (amount > this._entities.length) {
      return this._createEntities(amount - this._entities.length, this._entities.length)
    }
    return this._entities.slice(0, amount)
  }

  updateEntities(): EntityDTO[] {
    return this._updateEntities()
  }

  private _createEntities(amount: number, offset: number): EntityDTO[] {
    const childrenIds: string[] = faker.helpers.shuffle(Array.from({ length: amount }, (_, i) => `${i + 1 + offset}`))
    for (let i = 0; i < amount; i++) {
      const id = `${i + offset + 1}`
      const childId = childrenIds[i]
      const entity = generateEntity()
      const child = generateChild()
      this._entities[i + offset] = { id, ...entity, child: { id: childId, ...child } }
    }
    return this._entities
  }

  private _updateEntities(): EntityDTO[] {
    const update: EntityDTO[] = []
    for (let i = 0; i < this._requestedEntitiesAmount; i++) {
      const { child, ...entity } = this._entities[i]
      const childUpdate = generateChild()
      const entityUpdate = { ...entity, ...generateEntity(), child: { ...child, ...childUpdate } }
      this._entities[i] = entityUpdate
      update.push(entityUpdate)
    }
    return update
  }
}

