import { faker } from '@faker-js/faker'

import { EntityDTO } from '../entity.interface'
import { EntityGenerator } from './generator'


describe('EntityGenerator', () => {
  let generator: EntityGenerator
  let entities: EntityDTO[]

  beforeEach(() => {
    faker.seed(100)
    generator = new EntityGenerator()
    entities = [...generator.updateEntitiesAmount(500)]
  })

  describe('updateEntitiesAmount', () => {
    it('should generate 500 entities', () => {
      expect(entities.length).toEqual(500)
    })

    describe('On entities amount INCREASED', () => {
      let updatedEntities: EntityDTO[]

      beforeEach(() => {
        updatedEntities = generator.updateEntitiesAmount(700)
      })

      it('should return 700 entities', () => {
        expect(updatedEntities.length).toEqual(700)
      })

      it('the first 500 elements should stay the same', () => {
        entities.forEach((item, index) => {
          expect(item)
            .withContext(`entity #${index} should not change after amount update`)
            .toEqual(updatedEntities[index])
        })
      })

      it('additional entities should not duplicate existing', () => {
        updatedEntities.slice(500).forEach((item, index) => {
          expect(entities)
            .withContext(`duplication check for item #${index}`)
            .not.toContain(item)
        })
      })
    })

    describe('On entities amount REDUCED', () => {
      let updatedEntities: EntityDTO[]

      beforeEach(() => {
        updatedEntities = generator.updateEntitiesAmount(200)
      })

      it('should return 200 entities', () => {
        expect(updatedEntities.length).toEqual(200)
      })

      it('all elements should stay the same', () => {
        updatedEntities.forEach((item, index) => {
          expect(item)
            .withContext(`entity #${index} should not change after amount update`)
            .toEqual(entities[index])
        })
      })
    })
  })

  describe('updateEntities', () => {
    let updatedEntities: EntityDTO[]

    beforeEach(() => updatedEntities = generator.updateEntities())

    it('should regenerate 500 entities', () => {
      expect(updatedEntities.length).toEqual(500)

      updatedEntities.forEach((item, index) => {
        expect(item)
          .withContext(`entity #${index} should be updated`)
          .not.toEqual(entities[index])
      })
    })

    it('entities ids should not been changed', () => {
      updatedEntities.forEach((item, index) => {
        expect(item.id).toEqual(entities[index].id)
      })
    })

    it('entity`s children ids should not been changed', () => {
      updatedEntities.forEach((item, index) => {
        expect(item.child.id).toEqual(entities[index].child.id)
      })
    })
  })
})
