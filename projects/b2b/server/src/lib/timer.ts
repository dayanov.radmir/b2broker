import {
  BehaviorSubject,
  distinctUntilChanged,
  map,
  Observable,
  pairwise,
  startWith,
  switchMap,
  tap,
  timer,
} from 'rxjs'


export class Timer {
  private _interval$ = new BehaviorSubject<number>(300)

  updateInterval(interval: number): void {
    this._interval$.next(interval)
  }

  start(): Observable<unknown> {
    let lastEventTimestamp: number = Date.now()
    return this._interval$.asObservable()
      .pipe(
        startWith(0),
        pairwise(),
        distinctUntilChanged(),
        map((timers) => this._getTimerParams(timers, lastEventTimestamp)),
        switchMap(
          ([start, timeout]) => timer(start, timeout).pipe(
            tap(() => lastEventTimestamp = Date.now()),
          ),
        ),
      )
  }

  private _getTimerParams([prevTimeout, currentTimeout]: [number, number], lastEventTimestamp: number): [number, number] {
    switch (true) {
      case !prevTimeout:
        return [0, currentTimeout]

      case currentTimeout > prevTimeout: {
        const passed = Date.now() - lastEventTimestamp
        const startIn = currentTimeout - passed

        return [startIn, currentTimeout]
      }

      default: {
        const passed = Date.now() - lastEventTimestamp
        const startIn = passed > currentTimeout ? 0 : currentTimeout - passed

        return [startIn, currentTimeout]
      }

    }
  }
}
